#install.packages('cowplot')

# Bibliotecas que serão utilizadas
library(dplyr)
library(ggplot2)
library(tidyverse)
library(cowplot)

# Banco de dados com as populações. Previamente ajustado no excel
pop_to_1a <- read.csv("C:/Users/breno/Downloads/pop_to_1a.csv", sep=";")

# Mostra os tipos de cada variável
glimpse(pop_to_1a)



# QUESTÃO 1-A

# 1991
# Prepara o dataframe para cria as pirâmides etárias
Idade <- c()
for (j in (1:2)){
  Idade <- append(Idade, paste('00 a 04'))
  Idade <- append(Idade, paste('05 a 09'))
  for (i in seq(10,75,5)){
    Idade <- append(Idade, paste(i, 'a', i+4))
  }
  Idade <- append(Idade, paste('80 +'))
}
Sexo <- c()
Sinal <- c()
PopPerc <- c()
tabela <- filter(pop_to_1a, Ano == 1991)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 3:19){
    PopPerc <- append(PopPerc, round(tabela[j,i]/tabela[3,20]*100*sin, 2))
    Sexo <- append(Sexo, ifelse(j==1, 'Masculino', 'Feminino'))
    Sinal <- append(Sinal, sin)
  }  
}
# Cria o dataframe
df_pir_91 <- data.frame(Idade, Sexo, PopPerc, Sinal)

# Gráfico: pirâmide etária
ggplot(df_pir_91)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Tocantins - 1991',
       subtitle=paste('População Residente:', format(tabela[3,20],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')


# 2000
PopPerc <- c()
tabela <- filter(pop_to_1a, Ano == 2000)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 3:19){
    PopPerc <- append(PopPerc, round(tabela[j,i]/tabela[3,20]*100*sin, 2))
  }  
}
# Cria o dataframe
df_pir_00 <- data.frame(Idade, Sexo, PopPerc, Sinal)
# Gráfico: pirâmide etária
ggplot(df_pir_00)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Tocantins - 2000',
       subtitle=paste('População Residente:', format(tabela[3,20],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')


# 2010
PopPerc <- c()
tabela <- filter(pop_to_1a, Ano == 2010)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 3:19){
    PopPerc <- append(PopPerc, round(tabela[j,i]/tabela[3,20]*100*sin, 2))
  }  
}
# Cria o dataframe
df_pir_10 <- data.frame(Idade, Sexo, PopPerc, Sinal)
# Gráfico: pirâmide etária
ggplot(df_pir_10)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Tocantins - 2010',
       subtitle=paste('População Residente:', format(tabela[3,20],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')


# 2015
PopPerc <- c()
tabela <- filter(pop_to_1a, Ano == 2015)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 3:19){
    PopPerc <- append(PopPerc, round(tabela[j,i]/tabela[3,20]*100*sin, 2))
  }  
}
# Cria o dataframe
df_pir_15 <- data.frame(Idade, Sexo, PopPerc, Sinal)
# Gráfico: pirâmide etária
ggplot(df_pir_15)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Tocantins - 2015',
       subtitle=paste('População Residente:', format(tabela[3,20],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')


# 2020
PopPerc <- c()
tabela <- filter(pop_to_1a, Ano == 2020)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 3:19){
    PopPerc <- append(PopPerc, round(tabela[j,i]/tabela[3,20]*100*sin, 2))
  }  
}
# Cria o dataframe
df_pir_20 <- data.frame(Idade, Sexo, PopPerc, Sinal)
# Gráfico: pirâmide etária
ggplot(df_pir_20)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Tocantins - 2020',
       subtitle=paste('População Residente:', format(tabela[3,20],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')


# 2030
PopPerc <- c()
tabela <- filter(pop_to_1a, Ano == 2030)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 3:19){
    PopPerc <- append(PopPerc, round(tabela[j,i]/tabela[3,20]*100*sin, 2))
  }  
}
# Cria o dataframe
df_pir_30 <- data.frame(Idade, Sexo, PopPerc, Sinal)
# Gráfico: pirâmide etária
ggplot(df_pir_30)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Tocantins - 2030',
       subtitle=paste('População Residente:', format(tabela[3,20],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')

