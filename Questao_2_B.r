
library(dplyr)
library(tidyverse)

# Bancos de dados
censo_pop <- read.csv("C:/Users/breno/Downloads/pop_to_10_cen-csv.csv", sep=";")
glimpse(censo_pop)
proj_pop_11.15 <- read.csv("C:/Users/breno/Downloads/proj_pop_11-15.csv", sep=";")
proj_pop_16.20 <- drop_na(read.csv("C:/Users/breno/Downloads/proj_pop_16-20.csv", sep=";"))
obitos_to_11.15 <- read.csv("C:/Users/breno/Downloads/obitos_to_11-15.csv", sep=";")
obitos_to_16.20 <- read.csv("C:/Users/breno/Downloads/obitos_to_16-20.csv", sep=";")

# Taxas importantes. Tabelas no excel
nasc_11_15 <- 123603
nasc_16_20 <- 122463
pop_fert_11_15 <- 4082858
pop_fert_16_20 <- 4380817
tfg_11_15 <- nasc_11_15 / pop_fert_11_15
tfg_16_20 <- nasc_16_20 / pop_fert_16_20

# Cria o banco de dados que receberá as nMx e nqx
columns = c('Idade', 'nMx', 'nqx') 
# 11-15
df_tv_11_15 = data.frame(matrix(nrow = 17, ncol = length(columns))) 
colnames(df_tv_11_15) = columns
df_tv_11_15$Idade <- censo_pop$Faixa.Etaria.detalhada[1:17]
# 16-20
df_tv_16_20 = data.frame(matrix(nrow = 17, ncol = length(columns))) 
colnames(df_tv_16_20) = columns
df_tv_16_20$Idade <- censo_pop$Faixa.Etaria.detalhada[1:17]


# Adiciona as nMx e nqx
for (i in (1:17)){
  df_tv_11_15[i,2] <- obitos_to_11.15[i,6] / proj_pop_11.15[i,7]
  df_tv_11_15[i,3] <- 5*df_tv_11_15[i,2]/(1 + 2.5*df_tv_11_15[i,2])
  df_tv_16_20[i,2] <- obitos_to_16.20[i,6] / proj_pop_16.20[i,7]
  df_tv_16_20[i,3] <- 10*df_tv_16_20[i,2]/(2 + 5*df_tv_16_20[i,2])
}
#df_tv_11_15[17,3] <- 1
#df_tv_16_20[17,3] <- 1
df_tv_11_15$npx <- 1 - df_tv_11_15$nqx
df_tv_16_20$npx <- 1 - df_tv_16_20$nqx

# Cria o banco de dados que receberá as projeções populacionais
columns = c('Idade', 2010:2020) 
df_proj_pop = data.frame(matrix(nrow = 17, ncol = length(columns))) 
colnames(df_proj_pop) = columns

# Preenche as colunas iniciais
df_proj_pop$Idade <- censo_pop$Faixa.Etaria.detalhada[1:17]
df_proj_pop$`2010` <- censo_pop$Total[1:17]

round(sum(df_proj_pop$`2010`[4:10])*tfg_11_15*5)
df_proj_pop$`2010`[3:10]

# Projeta a pop para 2015
df_proj_pop[1,7] <- round(sum(df_proj_pop[(4:10),2])*tfg_11_15*5)
for (j in (2:16)){
  df_proj_pop[j,7] <- round(df_proj_pop[j-1,2] * df_tv_11_15[j-1,4])
}
df_proj_pop[17,7] <- round((df_proj_pop[16,2]+df_proj_pop[17,2]) * (df_tv_11_15[16,4]+df_tv_11_15[17,4])/2)
# Projeta a pop para 2011 a 2014
for (i in (3:6)){
  for (j in (1:17)){
    df_proj_pop[j,i] <- round((df_proj_pop[j,2]*(5-(i-2)) + df_proj_pop[j,7]*(i-2)) /5)
  } 
}

# Projeta a pop para 2020
df_proj_pop[1,12] <- round(sum(df_proj_pop[(4:10),7])*tfg_16_20*5)
for (j in (2:16)){
  df_proj_pop[j,12] <- round(df_proj_pop[j-1,7] * df_tv_16_20[j-1,4])
}
df_proj_pop[17,12] <- round((df_proj_pop[16,7]+df_proj_pop[17,7]) * (df_tv_16_20[16,4]+df_tv_16_20[17,4])/2)
# Projeta a pop para 2016 a 2019
for (i in (8:11)){
  for (j in (1:17)){
    df_proj_pop[j,i] <- round((df_proj_pop[j,7]*(5-(i-7)) + df_proj_pop[j,12]*(i-7)) /5)
  } 
}

# Adiciona uma linha com a pop total de cada ano
df_proj_pop[18,1] <- 'Total'
for (i in (2:12)){
  df_proj_pop[18,i] <- sum(df_proj_pop[(1:17),i])
}

