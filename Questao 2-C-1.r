
library(dplyr)
library(ggplot2)
library(tidyverse)
library(cowplot)

pop_br <- read.csv("C:/Users/breno/Downloads/pop_br-csv.csv", sep=";")

glimpse(pop_br)
pop_br[1,1] <- " 0-4"
pop_br[2,1] <- " 5-9"
pop_br$Masculino <- as.integer(gsub("[.]", "", pop_br$Masculino))
pop_br$Feminino <- as.integer(gsub("[.]", "", pop_br$Feminino))
pop_br$Total <- as.integer(gsub("[.]", "", pop_br$Total))

# DF da pirâmide de 2010
PopPerc <- c()
Sexo <- c()
Sinal <- c()
tabela <- filter(pop_br, Ano == 2010)
Idade <- c()
for (j in (1:2)){
  Idade <- append(Idade, tabela$Idade[1:19])
  sin <- ifelse(j==1, -1, 1)
  for (i in 1:19){
    PopPerc <- append(PopPerc, round(tabela[i,j+1]/tabela[20,4]*100*sin, 2))
    Sexo <- append(Sexo, ifelse(j==1, 'Masculino', 'Feminino'))
    Sinal <- append(Sinal, sin)
  }
}
df_pir_br_10 <- data.frame(Idade, Sexo, PopPerc, Sinal)

# Gráfico: pirâmide etária
ggplot(df_pir_br_10)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Brasil - 2010',
       subtitle=paste('População Residente:', format(tabela[20,4],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')



# DF da pirâmide de 2020
PopPerc <- c()
tabela <- filter(pop_br, Ano == 2020)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 1:19){
    PopPerc <- append(PopPerc, round(tabela[i,j+1]/tabela[20,4]*100*sin, 2))
  }
}
df_pir_br_20 <- data.frame(Idade, Sexo, PopPerc, Sinal)

# Gráfico: pirâmide etária
ggplot(df_pir_br_20)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Brasil - 2020',
       subtitle=paste('População Residente:', format(tabela[20,4],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')


# DF da pirâmide de 2060
PopPerc <- c()
tabela <- filter(pop_br, Ano == 2060)
for (j in (1:2)){
  sin <- ifelse(j==1, -1, 1)
  for (i in 1:19){
    PopPerc <- append(PopPerc, round(tabela[i,j+1]/tabela[20,4]*100*sin, 2))
  }
}
df_pir_br_60 <- data.frame(Idade, Sexo, PopPerc, Sinal)

# Gráfico: pirâmide etária
ggplot(df_pir_br_60)+
  geom_bar(aes(x=Idade,y=PopPerc,fill=Sexo),stat='identity')+
  geom_text(aes(x=Idade,y=PopPerc+Sinal*.3,label=abs(PopPerc)))+
  coord_flip()+
  scale_fill_manual(name='',values=c('darkred','steelblue'))+
  scale_y_continuous(breaks=seq(-10,10,1),
                     labels=function(x){paste(abs(x),'%')})+
  labs(x='',y='População (%)',
       title='Pirâmide Etária - Brasil - 2060',
       subtitle=paste('População Residente:', format(tabela[20,4],big.mark='.')),)+
  cowplot::theme_cowplot()+
  theme(axis.text.x=element_text(vjust=.5),
        panel.grid.major.y = element_line(color='lightgray',linetype='dashed'),
        legend.position = 'top',
        legend.justification = 'center')
