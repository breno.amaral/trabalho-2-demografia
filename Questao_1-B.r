
library(dplyr)
library(ggplot2)
library(tidyverse)

# QUESTÃO 1-B

# Proporção de idosos
# 1991
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 1991)
prop_ido_91 <- sum(lista[15:19])/lista$Total
prop_ido_91 # 0.05638992

# 2000
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2000)
prop_ido_00 <- sum(lista[15:19])/lista$Total
prop_ido_00 # 0.06776608

# 2010
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2010)
prop_ido_10 <- sum(lista[15:19])/lista$Total
prop_ido_10 # 0.08497194

# 2015
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2015)
prop_ido_15 <- sum(lista[15:19])/lista$Total
prop_ido_15 # 0.08573808

# 2020
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2020)
prop_ido_20 <- sum(lista[15:19])/lista$Total
prop_ido_20 # 0.1001616

# 2030
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2030)
prop_ido_30 <- sum(lista[15:19])/lista$Total
prop_ido_30 # 0.1387209


# Proporção de crianças
# 1991
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 1991)
prop_cri_91 <- lista$X0.a.4.anos/lista$Total
prop_cri_91 # 0.1358267

# 2000
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2000)
prop_cri_00 <- lista$X0.a.4.anos/lista$Total
prop_cri_00 # 0.1160187

# 2010
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2010)
prop_cri_10 <- lista$X0.a.4.anos/lista$Total
prop_cri_10 # 0.08869814

# 2015
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2015)
prop_cri_15 <- lista$X0.a.4.anos/lista$Total
prop_cri_15 # 0.08655452

# 2020
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2020)
prop_cri_20 <- lista$X0.a.4.anos/lista$Total
prop_cri_20 # 0.07659462

# 2030
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2030)
prop_cri_30 <- lista$X0.a.4.anos/lista$Total
prop_cri_30 # 0.06414454


# Proporção de jovens
# 1991
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 1991)
prop_jov_91 <- sum(lista[3:5])/lista$Total
prop_jov_91 # 0.4169556

# 2000
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2000)
prop_jov_00 <- sum(lista[3:5])/lista$Total
prop_jov_00 # 0.3520635

# 2010
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2010)
prop_jov_10 <- sum(lista[3:5])/lista$Total
prop_jov_10 # 0.2876674

# 2015
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2015)
prop_jov_15 <- sum(lista[3:5])/lista$Total
prop_jov_15 # 0.277402

# 2020
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2020)
prop_jov_20 <- sum(lista[3:5])/lista$Total
prop_jov_20 # 0.2464153

# 2030
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2030)
prop_jov_30 <- sum(lista[3:5])/lista$Total
prop_jov_30 # 0.2013506


# Razão de dependência
# 1991
prop_rd_91 <- (prop_jov_91 + prop_ido_91) / (1-prop_jov_91-prop_ido_91)
prop_rd_91 # 0.898778

# 2000
prop_rd_00 <- (prop_jov_00 + prop_ido_00) / (1-prop_jov_00-prop_ido_00)
prop_rd_00 # 0.7236316

# 2010
prop_rd_10 <- (prop_jov_10 + prop_ido_10) / (1-prop_jov_10-prop_ido_10)
prop_rd_10 # 0.5939794

# 2015
prop_rd_15 <- (prop_jov_15 + prop_ido_15) / (1-prop_jov_15-prop_ido_15)
prop_rd_15 # 0.570204

# 2020
prop_rd_20 <- (prop_jov_20 + prop_ido_20) / (1-prop_jov_20-prop_ido_20)
prop_rd_20 # 0.530402

# 2030
prop_rd_30 <- (prop_jov_30 + prop_ido_30) / (1-prop_jov_30-prop_ido_30)
prop_rd_30 # 0.5153156


# Índice de envelhecimento
# 1991
prop_ie_91 <- prop_ido_91 / prop_jov_91 *100
prop_ie_91 # 13.5242

# 2000
prop_ie_00 <- prop_ido_00 / prop_jov_00 *100
prop_ie_00 # 19.24825

# 2010
prop_ie_10 <- prop_ido_10 / prop_jov_10 *100
prop_ie_10 # 29.53826

# 2015
prop_ie_15 <- prop_ido_15 / prop_jov_15 *100
prop_ie_15 # 30.90752

# 2020
prop_ie_20 <- prop_ido_20 / prop_jov_20 *100
prop_ie_20 # 40.64746

# 2030
prop_ie_30 <- prop_ido_30 / prop_jov_30 *100
prop_ie_30 # 68.89517


# Média
# 1991
acumulado <- 0
contador <- 3
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 1991)
for (i in seq(0, 75, 5)){
  acumulado <- acumulado + lista[contador] * (i+2.5)
  print(lista[contador])
  contador <- contador +1
}
# Cálculo da idade média do intervalo 80+
# Pega a proporção média de homens e mulheres para este intervalo
pop_80_masc <- mean(filter(pop_to_1a, Sexo=='Masculino')$X80.anos.e.mais) /
  ( mean(filter(pop_to_1a, Sexo=='Masculino')$X80.anos.e.mais) +
      mean(filter(pop_to_1a, Sexo=='Feminino')$X80.anos.e.mais) )
pop_80_fem <- 1 - pop_80_masc
# Multiplica as proporções pela +Kx da tábua de vida de cada sexo (obtida no trabalho 1)
k_80 <- pop_80_masc*2.738 + pop_80_fem*5.598
# Soma a idade média do último intervalo e calcula a média
acumulado <- acumulado + lista$X80.anos.e.mais * (80+k_80)  
media_91 <- as.numeric(acumulado/lista$Total)
media_91 # 23.6932

# 2000
acumulado <- 0
contador <- 3
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2000)
for (i in seq(0, 75, 5)){
  acumulado <- acumulado + lista[contador] * (i+2.5)
  contador <- contador +1
}
acumulado <- acumulado + lista$X80.anos.e.mais * (80+k_80)  
media_00 <- as.numeric(acumulado/lista$Total)
media_00 # 25.89592

# 2010
acumulado <- 0
contador <- 3
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2010)
for (i in seq(0, 75, 5)){
  acumulado <- acumulado + lista[contador] * (i+2.5)
  contador <- contador +1
}
acumulado <- acumulado + lista$X80.anos.e.mais * (80+k_80)  
media_10 <- as.numeric(acumulado/lista$Total)
media_10 # 29.11447

# 2015
acumulado <- 0
contador <- 3
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2015)
for (i in seq(0, 75, 5)){
  acumulado <- acumulado + lista[contador] * (i+2.5)
  contador <- contador +1
}
acumulado <- acumulado + lista$X80.anos.e.mais * (80+k_80)  
media_15 <- as.numeric(acumulado/lista$Total)
media_15 # 29.72715

# 2020
acumulado <- 0
contador <- 3
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2020)
for (i in seq(0, 75, 5)){
  acumulado <- acumulado + lista[contador] * (i+2.5)
  contador <- contador +1
}
acumulado <- acumulado + lista$X80.anos.e.mais * (80+k_80)  
media_20 <- as.numeric(acumulado/lista$Total)
media_20 # 31.5297

# 2030
acumulado <- 0
contador <- 3
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2030)
for (i in seq(0, 75, 5)){
  acumulado <- acumulado + lista[contador] * (i+2.5)
  contador <- contador +1
}
acumulado <- acumulado + lista$X80.anos.e.mais * (80+k_80)  
media_30 <- as.numeric(acumulado/lista$Total)
media_30 # 35.21819


# Idade mediana
# 1991
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 1991)
logico <- T
contador <- 3
while (logico==T){
  if (sum(lista[3:contador]) < lista$Total/2){
    contador <- contador + 1
  }else{
    mediana_91 <- as.numeric(( (lista$Total/2-sum(lista[3:contador-1]))/lista[contador] + (contador-3) ) * 5)
    logico <- F
  }
}
mediana_91 # 18.5729

# 2000
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2000)
logico <- T
contador <- 3
while (logico==T){
  if (sum(lista[3:contador]) < lista$Total/2){
    contador <- contador + 1
  }else{
    mediana_00 <- as.numeric(( (lista$Total/2-sum(lista[3:contador-1]))/lista[contador] + (contador-3) ) * 5)
    logico <- F
  }
}
mediana_00 # 21.50758

# 2010
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2010)
logico <- T
contador <- 3
while (logico==T){
  if (sum(lista[3:contador]) < lista$Total/2){
    contador <- contador + 1
  }else{
    mediana_10 <- as.numeric(( (lista$Total/2-sum(lista[3:contador-1]))/lista[contador] + (contador-3) ) * 5)
    logico <- F
  }
}
mediana_10 # 25.82112

# 2015
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2015)
logico <- T
contador <- 3
while (logico==T){
  if (sum(lista[3:contador]) < lista$Total/2){
    contador <- contador + 1
  }else{
    mediana_15 <- as.numeric(( (lista$Total/2-sum(lista[3:contador-1]))/lista[contador] + (contador-3) ) * 5)
    logico <- F
  }
}
mediana_15 # 27.07451

# 2020
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2020)
logico <- T
contador <- 3
while (logico==T){
  if (sum(lista[3:contador]) < lista$Total/2){
    contador <- contador + 1
  }else{
    mediana_20 <- as.numeric(( (lista$Total/2-sum(lista[3:contador-1]))/lista[contador] + (contador-3) ) * 5)
    logico <- F
  }
}
mediana_20 # 29.27857

# 2030
lista <- filter(pop_to_1a, Sexo == 'Total', Ano == 2030)
logico <- T
contador <- 3
while (logico==T){
  if (sum(lista[3:contador]) < lista$Total/2){
    contador <- contador + 1
  }else{
    mediana_30 <- as.numeric(( (lista$Total/2-sum(lista[3:contador-1]))/lista[contador] + (contador-3) ) * 5)
    logico <- F
  }
}
mediana_30 # 33.76869


# Razão de sexo (2010 e 2030)

# Prepara vetores para criar o dataframe que gera o gráfico
RS <- c()
Ano <- c()
for (j in c(2010, 2030)){
  tabela <- filter(pop_to_1a, Ano == j)
  for (i in (3:19)){
    RS <- append(RS, tabela[1,i]/tabela[2,i])
    Ano <- append(Ano, as.character(j))
  }
}
Idade <- c()
for (j in (1:2)){
  Idade <- append(Idade, paste('00 a 04'))
  Idade <- append(Idade, paste('05 a 09'))
  for (i in seq(10,75,5)){
    Idade <- append(Idade, paste(i, 'a', i+4))
  }
  Idade <- append(Idade, paste('80 +'))
}

# Cria o dataframe
df_rs <- data.frame(Ano, Idade, RS)
df_rs

# Gera o gráfico
df_rs %>%
  ggplot( aes(x=Idade, y=RS, group=Ano, color=Ano)) +
  geom_line(size=1) +
  #ggtitle("Razão de Sexos (RS)") +
  ylab("Valor da RS") +
  xlab("Idade em anos completos") +
  geom_hline(aes(yintercept = 1), color='#67C91A') +
  ylim(0.8,1.2)

