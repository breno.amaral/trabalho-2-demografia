# O 1º trecho do código apenas trata o banco de dados e o exporta para csv e excel

#install.packages('writexl')

# Bibliotecas utilizadas
library(writexl)
library(dplyr)
library(ggplot2)
library(tidyverse)
library(cowplot)


# Importa o DF
pop_idade_simples <- read.csv("C:/Users/breno/Downloads/pop_idade_simples-bruto.csv", sep=";")

# Mostra os tipos de variáveis do BD
glimpse(pop_idade_simples)

# Retira variáveis desnecessárias
pop_idade_simples <- pop_idade_simples[1:4]

# Função que retira o espaço no início de algumas idades
tira_esp_ini <- function(texto){
  return( ifelse( (substr(texto, 1, 2)=='  '), substr(texto, 3, nchar(texto)), texto ) )
}

# Aplica as adaptações nas variáveis
pop_idade_simples$Idade <- tira_esp_ini(pop_idade_simples$Idade)
# Transforma as variáveis em inteiro
pop_idade_simples$Total <- as.integer(gsub(" ", "", pop_idade_simples$Total))
pop_idade_simples$Masculino <- as.integer(gsub(" ", "", pop_idade_simples$Masculino))
pop_idade_simples$Feminino <- as.integer(gsub(" ", "", pop_idade_simples$Feminino))

# Sumariza novamente para vermos as alterações feitas
glimpse(pop_idade_simples)

# Exporta o banco de dados para csv e excel, respectivamente
write.csv(pop_idade_simples, "pop_idade_simples-csv", row.names = T)
write_xlsx(pop_idade_simples, path = "C:/Users/breno/Downloads/pop_idade_simples-excel.xlsx")


# A partir daqui, o código refere-se à construção da pirâmide

# Comando que importa e retira a variável adicional (inútil) colocada no csv
pop_idade_simples <- read.csv("C:/Users/breno/Downloads/pop_idade_simples-csv")
pop_idade_simples <- subset(pop_idade_simples, select = -c(X) )

# Prepara o DF que será utilizado para gerar a pirâmide
Sexo <- c()
PopPerc <- c()
Sinal <- c()
for (j in (1:2)){
  gen <- ifelse((j==1), 'Masculino', 'Feminino')
  sin <- ifelse((j==1), -1, 1)
  for (i in (1:101)){
    Sexo <- append(Sexo, gen)
    PopPerc <- append(PopPerc, round(pop_idade_simples[i,j+2]/pop_idade_simples[102, 2]*100*sin, 3))
    Sinal <- append(Sinal, sin)
  }
}
Idade <- c()
for (j in (1:2)){
  Idade <- append(Idade, paste('  ', pop_idade_simples$Idade[1]))
  for (i in (2:10)){
    Idade <- append(Idade, paste(' ', pop_idade_simples$Idade[i]))
  }
  for (i in (11:100)){
    Idade <- append(Idade, substr( paste(' ', pop_idade_simples$Idade[i]), 2, 9))
  }
  Idade <- append(Idade, pop_idade_simples$Idade[101])
}
df_pir_cen <- data.frame(Idade, Sexo, PopPerc, Sinal)

# Cria vetor que contém a legenda do eixo Y no gráfico
texto_eixo <- c()
for (i in (0:100)){
  if (i%%5 == 0){
    texto_eixo <- append(texto_eixo, Idade[i+1])
  }else{
    texto_eixo <- append(texto_eixo, ' ')
  }
}


# Gera a pirâmide
df_pir_cen %>%
  ggplot( aes(x=Idade, y=PopPerc, group=Sexo, color=Sexo)) +
  geom_line(size=1) +
  coord_flip()+
  theme_test()+
  geom_hline(yintercept = 0, color='black') +
  geom_vline(xintercept = seq(1, 101, 5), linetype="dashed", color='grey') +
  theme(axis.ticks.y = element_blank())+
  scale_y_continuous(breaks=seq(-1.1, 1.1, 0.1),
                     labels=function(x){paste(abs(x),'%')})+
  scale_x_discrete(labels = texto_eixo)+
  labs(x='',y='População (%)',
       title='Pirâmide Etária por Idade Simples - Tocantins - 2010',
       subtitle=paste('População Residente:', format(pop_idade_simples[102,2],big.mark='.')),)
  
